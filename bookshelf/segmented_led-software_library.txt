Overview
  The Segmented LED project contains a software library component that includes all the code necessary to compile a library that will allow an Arduino to control one or more chains of modules.

Technologies
  This software libary is written in C using the Arduino software library.

Design
  All global tokens defined in this project will be prefixed with "segled"
  Local Constants
    Array of unsigned chars that contain the mappings for all 16 digits
      Each bit in each byte of the array maps to a single LED segment
        1   Upper right
        2   Top
        4   Upper left
        8   Lower left
        16  Bottom
        32  Lower right
        64  Center
        128 Decimal point
     Decimal Point Mask = 128
     Segments Per Cluster = 8
     Max Base = 16

  Global Constants
    Enumeration of Return Values
      Success
      Error Module Exists
      Error Pin Settings Array Not Allocated
      Error Allocation Failed
      Error Pin Settings Already Allocated
      Error All Pin Settings Defined
      Error Pin Settings Do Not Exist
      Error Argument Is Not Non Negative Integer
      Error Base Is Too Large

  Global
    Number of Pin Settings defined for module chains
    Maximum Pin Settings

  Local
    Pin Settings Array

  Struct 
    Pin Settings for a module chain
      shcp
      stcp
      ds
  
  Function Set Maximum Module Chains
    Arguments
      The maximum number of module chains
    Returns
      Success
      Error Allocation Failed
      Error Pin Settings Already Allocated
    Pre-conditions
      Pin Settings Array is not allocated
      Maximum Pin Settings is 0
    Post-conditions
      If Success was returned
        The next N calls to Add Module Chain will be successful where N is the argument
      If Error Allocation Failed was returned
        Calls to Add Module Chain, Set Segments, and Output Number will return Error Pin Settings Array Not Allocated
    Logic
      If Maximum Pin Settings is not 0
        Return Error Pin Settings Already Allocated
      Allocate a new Pin Settings Array that is the size of the argument
      If the allocation failed
        Return Error Allocation Failed
      Return Success

  Function Add Module Chain
    Arguments
      Struct of the pin settings to add
    Returns
      Error Module Chain Exists
      Error All Pin Settings Defined
      An identifier for the module chain pin settings that corresponds to its index
    Pre-conditions
      The pin settings are unique
      Set Maximum Module Chains has been called successfully
    Post-conditions
      Passing the returned identifier into the Set Segments or Output Number functions cause the input into those functions to go to the identified module chain
    Logic
      If Number of Pin Settings is equal to Maximum Pin Settings
        Return Error All Pin Settings Defined
      For each entry in the pin settings array
        If the sh, st, or ds members of the struct argument match the struct array entry
          Return Error Module Chain Exists
      Copy the argument to the Pin Settings Array element at index Number of Pin Settings
      Return Number of Pin Settings and post-increment it

  Local Function Flip Registers
    Arguments
      Identifier for the module chain to flip the registers for
    Returns
      Error Pin Settings Do Not Exist
    Pre-conditions
      The Identifier argument must be less than the Number of Pin Settings
    Post-conditions
      The LEDs are illuminated to what was last pushed into the registers
    Logic
      If the Identifier argument is greater than or equal to the Number of Pin Settings
        Return Error Pin Settings Do Not Exist
      Set the appropriate STCP pin to HIGH
      Set the appropriate STCP pin to LOW

  Function Push Segment Data
    Arguments
      Identifier for the module chain to push the segment data to
      Segment Data that conforms to the same format as the digit mappings
    Returns
      Success
      Error Pin Settings Do Not Exist
    Pre-conditions
      The Identifier argument must be less than the Number of Pin Settings
    Post-conditions
      The registers in the module chain are loaded according to the Array argument
    Logic
      If the Identifier argument is greater than or equal to the Number of Pin Settings
        Return Error Pin Settings Do Not Exist
      For each segment in a cluster
        If the segment data from the Array is 0
          Set the appropriate DS pin to HIGH
        else
          Set the appropriate DS pin to LOW
        Set the appropriate SHCP pin to HIGH 
        Set the appropriate SHCP pin to LOW
      Return Success

  Function Push Number
    Arguments
      Identifier for the module chain to push the segment data to
      Number to output
      Base
    Returns
      Success
      Error Pin Settings Do Not Exist
      Error Argument Is Not Non Negative Integer
      Error Base Is Too Large
    Pre-conditions
      Number argument must be a non-negative integer
    Post-conditions
      The registers in the module chain are loaded according to the Number argument
    Logic
      If the Identifier argument is greater than or equal to the Number of Pin Settings
        Return Error Pin Settings Do Not Exist
      If the Number argument is less than 0
        Return Error Argument Is Not Non Negative Integer
      If the Base argument is greater than the Max Base
        Return Error Base Is Too Large
      Current Number <- the Number argument
      While the Current Number is greater than 0
        Call Push Segment Data passing the following
          Identifier argument
          Number divided by the Base argument
        Current Number <- Current Number modulus the Base argument  

Requirements
  
