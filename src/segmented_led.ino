///////////////////////
//  LIBRARY FUNCTIONS
///////////////////////
int i_pow( int base, int exponent ) {
  int total = 1;
  for( int i = 0; i < exponent; i++ ) {
    total *= base;
  }
  return total;
}

//////////////
// CONSTANTS
//////////////
const float MAX_INPUT_VALUE = 1024.0f;

const int DS_PIN = 2;
const int STCP_PIN = 3;
const int SHCP_PIN = 4;

const int LED_COUNT = 8;
const int MAX_OUTPUT_VALUE = 100;
const byte INPUT_PIN = 0;
const int DELAY_MS = 500;

// 1 0 1 0 1 1 1 1

// { 0, 0, 0, 0, 0, 0, 0 },
// 2 3 4 5 6 7 8 9
// 0 1 2 3 4 5 6 7
// E D C B A G F DP
const byte SEG_LED_OUT[10][7] = {
  // 0 A B C D E F
  // 0 4 3 2 1 0 6
  { 1, 1, 1, 1, 1, 0, 1 },
  // 1 B C
  // 1 3 2
  { 0, 0, 1, 1, 0, 0, 0 },
  // 2 A B G E D
  // 2 4 3 5 0 1
  { 1, 1, 0, 1, 1, 1, 0 },
  // 3 A B G C D
  // 3 4 3 5 2 1
  { 0, 1, 1, 1, 1, 1, 0 },
  // 4 F B G C
  // 4 6 3 5 2
  { 0, 0, 1, 1, 0, 1, 1 },
  // 5 A F G C D
  // 5 4 6 5 2 1
  { 0, 1, 1, 0, 1, 1, 1 },
  // 6 A F G C D E
  // 6 4 6 5 2 1 0
  { 1, 1, 1, 0, 1, 1, 1 },
  // 7 B C A
  // 7 3 2 4
  { 0, 0, 1, 1, 1, 0, 0 },
  // 8 A B C D E F G
  // 8 4 3 2 1 0 6 5
  { 1, 1, 1, 1, 1, 1, 1 },
  // 9 B C A F G
  // 9 3 2 4 6 5
  { 0, 0, 1, 1, 1, 1, 1 }
};

/////////////
//  PROGRAM
/////////////
void setup() {
  pinMode( DS_PIN, OUTPUT );
  digitalWrite( DS_PIN, LOW );
  pinMode( SHCP_PIN, OUTPUT );
  digitalWrite( SHCP_PIN, LOW );
  pinMode( STCP_PIN, OUTPUT );
  digitalWrite( STCP_PIN, LOW );
  // pinMode( INPUT_PIN, INPUT );
  Serial.begin( 9600 );
  Serial.println( "Serial output started" );
}

void output( byte value, boolean decimal ) {
  for( int i = 0; i < LED_COUNT - 1; i++ ) {
    if( SEG_LED_OUT[ value ][ i ] == 0 ) {
      digitalWrite( DS_PIN, HIGH );
    } else {
      digitalWrite( DS_PIN, LOW );
    }
    digitalWrite( SHCP_PIN, HIGH );  // Shift a bit
    digitalWrite( SHCP_PIN, LOW );
  }
  if( decimal )  {
    digitalWrite( DS_PIN, LOW );
  } else {
    digitalWrite( DS_PIN, HIGH );
  }
  digitalWrite( SHCP_PIN, HIGH ); // Shift a bit
  digitalWrite( SHCP_PIN, LOW );
  
}

void switch_register() {
  digitalWrite( STCP_PIN, HIGH );  // Switch register to output
  digitalWrite( STCP_PIN, LOW );
}

void loop() {
  static int timer_ms = 0;
  static int reference_ms = 0;
  static int count = 0;
  
  float analog_value = ( analogRead( INPUT_PIN ) / MAX_INPUT_VALUE ) *
                       MAX_OUTPUT_VALUE;
                       
  output( ( (int) analog_value ) % 10, false );
  output( analog_value / 10, false );
  switch_register();
  
  timer_ms = millis() - reference_ms;
  if( timer_ms >= 1000 ) {
    Serial.print( "LPS " );
    Serial.println( count );
    reference_ms = millis();
    count = 0;
  }
  count++;
}
